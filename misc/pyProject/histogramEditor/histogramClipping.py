# coding=iso-8859-15
'''

By twipOS 2014.
'''
from itom import userIsDeveloper
from itom import userIsAdmin
from itom import dataObject
from itom import ui
from itomUi import ItomUi
import inspect
import os.path
import numpy as np
from itom import filter
from numpy import NaN

hasBASICFILTERS = True
if not (itom.pluginLoaded("BasicFilters")):
    loadWarnings.append("- Not all functions available since plugin 'BasicFilters' not available.")
    hasBASICFILTERS = False

class histogramClipping(ItomUi):
    
    ownFilename = ""
    ownDir = ""
    upDating = True
    haseMCPP = False
    colTVec = dataObject.zeros([256], dtype='float64')
    imageOrig = dataObject()
    imageMod = dataObject()
    scale = 1.0
    offset = 0.0
    
    def __init__(self, hasBASICFILTERS, systemPath = None):
        self.upDating = True
        self.hasMCPP = hasBASICFILTERS
        
        if(systemPath is None):
            ownFilename = inspect.getfile(inspect.currentframe())
            ownDir = os.path.dirname(os.path.realpath(__file__)) #os.path.dirname(ownFilename)
        else:
            ownDir = systemPath
        
        uiFile = os.path.join(ownDir, "ui/histogramClipping.ui")
        uiFile = os.path.abspath(uiFile)
        ItomUi.__init__(self, uiFile, ui.TYPEDIALOG, childOfMainWindow=True)
        
    def init(self, skipBox, defaultVarName):
        self.updateDObj(skipBox, defaultVarName)
        
        self.upDating = False
        return

    def getVarNameDialog(self, title, VarName, workSpace, objType = 'ND', acceptedObjectType = 1):
        '''
        getVarName(title, VarName, workSpace [, objType]) -> opens a drop down window with suitable DataObjects / npOjects / PointClouds and returns selected variable
        
        Parameters 
        ------------
        title : {str}
            Title of the dialog.
        VarName :  {str}
            default variable name.
        VarName :  {PythonDict}
            Kontent of the global workspace of caller (use globals())
        objType : {str}, optinal 
            ObjectTypes to filter. Can be ' line', 'plane' , ' empty' or 'ND'
        acceptedObjectType: {int}, optional
            BitSet 1 = DataObjects, 2 = NumPy-Array, 4 = PointClouds accepted
        
        
        Returns
        -------
        varname: {str}
            string value of the variable
        done:  {bool}
            Check, if dialog was closed with 'ok' or 'cancel'
        
        Notes
        -------
        This function opens a drop down window with suitable DataObject according to 'objType'.
        The selected Object-Name is returned together with a boolean expression representing
        the dialog status. If only one element fits to 'objType', the dialog is skipped.
        '''
        
        dObjects = []
        npObjects = []
        pcObjects = []
        
        key = ""
        objtype = 1
        dimsTestString = ''
        dimsNPTestString = ''
        
        try:
            if (acceptedObjectType & 2) == 2:
                eval("numpy.ndarray")
        except:
            import numpy
        
        try:
            if (acceptedObjectType & 4) == 4:
                eval("itom.PointCloud")
        except:
            acceptedObjectType = acceptedObjectType - 4
        
        if (acceptedObjectType & 1) == 1:
            if objType == 'empty':
                dimsTestString = '{}.dims == 0'
            elif objType == 'plane':
                dimsTestString = '{}.shape[{}.dims-1]>1 and {}.shape[{}.dims-2]>1'
            elif objType == 'line':
                dimsTestString = '{}.shape[{}.dims-1] == 1 or {}.shape[{}.dims-2] == 1'
            elif objType == 'lineORplane':
                dimsTestString = '{}.ndim > 0 and ({}.ndim < 3 or (({}.ndim < 4) and ({}.shape[0] == 1)))'
            else:
                dimsTestString = '{}.dims > 0'
                
        if (acceptedObjectType & 2) == 2:
            if objType == 'empty':
                dimsNPTestString = '{}.ndim == 0'
            elif objType == 'plane':
                dimsNPTestString = '{}.ndim > 1 and {}.shape[{}.ndim-1]>1 and {}.shape[{}.ndim-2]>1'
            elif objType == 'line':
                dimsNPTestString = '({}.ndim == 1) or ({}.shape[{}.ndim-1] == 1) or ({}.shape[{}.ndim-2] == 1)'
            elif objType == 'lineORplane':
                dimsNPTestString = '{}.ndim > 0 and ({}.ndim < 3 or (({}.ndim < 4) and ({}.shape[0] == 1)))'
            else:
                dimsNPTestString = '{}.ndim > 0'        
        
        for key in workSpace.keys():
            try:
                if ((acceptedObjectType & 1) == 1) and (type(workSpace[key]) == itom.dataObject):
                    try:
                        if eval(dimsTestString.format(key, key, key, key), workSpace):
                            dObjects.append(key)
                    except:
                        temp = 0
                        del temp
                        
                if ((acceptedObjectType & 2) == 2) and (type(workSpace[key]) == numpy.ndarray or type(workSpace[key]) == itom.npDataObject):
                    try:
                        if eval(dimsNPTestString.format(key, key, key, key, key), workSpace):
                            npObjects.append(key)
                    except:
                        temp = 0
                        del temp
            except:
                temp = 0
                del temp
        
        if (len(npObjects) < 1) and (len(pcObjects) < 1) and (len(dObjects) < 1):
            ui.msgCritical("Plot", "No dataObjects found", ui.MsgBoxOk)
            varname = ""
            done = False
            objtype = 0
            
        elif (len(npObjects) < 1) and (len(pcObjects) < 1):
            objtype = 1
            if len(dObjects) > 1:
                objindex = [i for i,x in enumerate(dObjects) if x == VarName]
                
                if len(objindex) > 0:
                    objindex = objindex[0]
                else:
                    objindex = 0
                
                [varname, done]= itom.ui.getItem(title, "DataObjects", dObjects, objindex, False)
            else:
                varname = dObjects[0]
                done = True

        elif (len(dObjects) < 1) and (len(pcObjects) < 1):
            objtype = 2
            if len(npObjects) > 1:
                objindex = [i for i,x in enumerate(npObjects) if x == VarName]
                
                if len(objindex) > 0:
                    objindex = objindex[0]
                else:
                    objindex = 0
                
                [varname, done]= itom.ui.getItem(title, "numpyArrays", npObjects, objindex, False)
            else:
                varname = npObjects[0]
                done = True
        else:
            allDics = []
            allDics.extend(dObjects)
            allDics.extend(npObjects)
            allDics.extend(pcObjects)
            
            objindex = [i for i,x in enumerate(allDics) if x == VarName]
            
            if len(objindex) > 0:
                objindex = objindex[0]
            else:
                objindex = 0
            
            [varname, done]= itom.ui.getItem(title, "numpyArrays", allDics, objindex, False)
            
            if varname in pcObjects:
                objtype = 4
            elif varname in npObjects:
                objtype = 2            
            else:
                objtype = 1

            del allDics
        
        del key
        del dObjects
        del npObjects
        del pcObjects
        del dimsTestString
        del dimsNPTestString
        
        return [varname, objtype, done]

    def updateDObj(self, skipBox, defaultVarName):
        if self.hasMCPP == False:
            ui.msgCritical("DataObject", "Execution error, MCPPFilterFuncs not loaded", ui.MsgBoxOk)
            check = False
            result = 0
            return [check, result]
        
        if defaultVarName == None:
            dataObj = self.defaultVarName
        else:
            dataObj = defaultVarName
        
        if skipBox == False:
            [dataObj, obtype, check] = self.getVarNameDialog("Choose Object:", dataObj, globals(), 'lineORplane', 1)
        else:
            check = True
        
        if check == True:
            try:
                # Check if variable exists
                eval(dataObj)
                self.selectedDObj = dataObj
            except:
                if skipBox == False:
                    ui.msgCritical("DataObject", "Variable does not exist", ui.MsgBoxOk)
                return [False, 0]
            try:
                self.imageOrig = eval(dataObj)
                #self.waveTxt = self.gui.waveTxt["html"]
        
                self.gui.image["source"] = self.imageOrig
                self.gui.image["colorMap"] = "gray"
                self.gui.image.setProperty({"title" : "Image"})
                if self.imageOrig.dtype == "uint8":
                    self.gui.image["zAxisInterval"] = [0,255]
        
                tempHist = dataObject()
                s = "filter(\"calcHist\",{}, tempHist)"
                eval(s.format(dataObj))
                self.histObj = tempHist
                tempHist2 = dataObject.zeros([tempHist.size()[0] + 1, tempHist.size()[1]], dtype=tempHist.dtype)
                tempHist2[0:tempHist.size()[0], :] = tempHist.copy()
                
                valRange = np.abs(np.max(self.imageOrig) - np.min(self.imageOrig))
                self.scale = valRange / tempHist2.shape[1]
                self.offset = np.min(self.imageOrig) / self.scale
                tempHist2.setAxisScale(1, self.scale)
                tempHist2.setAxisOffset(1, -self.offset)
                self.gui.histogram["source"] = tempHist2
                self.gui.histogram.setProperty({"title" : "Histogram"})
                [sy, sx] = self.imageOrig.shape
                self.avg = np.sum(self.imageOrig) / (sx * sy)
                self.sdev = np.sqrt(np.sum((self.imageOrig - self.avg) ** 2) / (sx * sy - 1))
                self.gui.dsbAvg["value"]  = self.avg
                self.gui.dsbSDev["value"]  = self.sdev
                self.gui.dsbLowerLimit["value"] = float(np.min(self.imageOrig))
                self.gui.dsbUpperLimit["value"] = float(np.max(self.imageOrig))
                #self.gui.histogram.setProperty({"yAxisInterval" : [0, 255]})
                #self.gui.histogram.setProperty({"xAxisInterval" : [0, 255]})
            except:
                if skipBox == False:
                    ui.msgCritical("DataObject", "Execution error", ui.MsgBoxOk)
                check = False
                result = 0
        else:
            result = 0
        self.gui.histogram.call("deletePicker", -1)
        if not self.gui.histogram["source"] is None:
            self.gui.histogram.call("appendPicker", [self.gui.histogram["xAxisInterval"].min, self.gui.histogram["xAxisInterval"].max])
        
    def show(self,modalLevel = 0):
        ret = self.gui.show(modalLevel)
    
    @ItomUi.autoslot("")
    def on_pushButtonCancel_clicked(self):
        self.gui.hide()
        global histClipping
        del histClipping
        return
    
    @ItomUi.autoslot("")
    def on_pushButtonAccept_clicked(self):
        exec("global " + self.selectedDObj + "\n" + self.selectedDObj + " = self.gui.image['source']")
        self.gui.hide()
        global histClipping
        del histClipping
        return
    
    @ItomUi.autoslot("int,double,double,int")
    def on_histogram_pickerChanged(self, idx, posx, posy, curveIdx):
        #limits = self.gui.histogram["picker"]
        #self.gui.dsbLowerLimit["value"] = limits[0,0]        
        #self.gui.dsbUpperLimit["value"] = limits[1,0]
        if (idx == 0):
            self.gui.dsbLowerLimit["value"] = posx
        elif (idx == 1):
            self.gui.dsbUpperLimit["value"] = posx
    
    def updateGraph(self):
        if not self.imageOrig is None:
            auxObj1 = self.imageOrig > self.gui.dsbLowerLimit["value"]
            auxObj2 = self.imageOrig <  self.gui.dsbUpperLimit["value"]
            self.imageMod = self.imageOrig.copy() 
            if self.imageOrig.dtype=='float64':
                auxObj1 = dataObject.ones([self.imageOrig.shape[0], self.imageOrig.shape[1]],dtype='float64').div(auxObj1.astype('float64')/255)
                auxObj2 = dataObject.ones([self.imageOrig.shape[0], self.imageOrig.shape[1]],dtype='float64').div(auxObj2.astype('float64')/255)
                self.imageMod = self.imageMod.mul(auxObj1)
                self.imageMod = self.imageMod.mul(auxObj2)
            else:
                auxObj1 = dataObject.ones([self.imageOrig.shape[0], self.imageOrig.shape[1]],dtype='float32').div(auxObj1.astype('float32')/255)
                auxObj2 = dataObject.ones([self.imageOrig.shape[0], self.imageOrig.shape[1]],dtype='float32').div(auxObj2.astype('float32')/255)
                self.imageMod = self.imageMod.astype('float32').mul(auxObj1)
                self.imageMod = self.imageMod.astype('float32').mul(auxObj2)
            self.gui.image["source"] = self.imageMod
        
    @ItomUi.autoslot("")
    def on_pbUpdate_clicked(self):
        limits = self.gui.histogram["picker"]
        self.gui.dsbLowerLimit["value"] = (limits[0,0] + self.offset) * self.scale
        self.gui.dsbUpperLimit["value"] = (limits[1,0] + self.offset) * self.scale
        self.updateGraph()
        
    def adjustLimitsSDev(self, nsdev):
        lowLim = self.avg - nsdev * self.sdev
        lowVal = np.min(self.imageOrig)
        if lowLim > lowVal:
            self.gui.dsbLowerLimit["value"] = float(lowLim)
        else:
            self.gui.dsbLowerLimit["value"] = float(lowVal)
        highLim = self.avg + nsdev * self.sdev
        highVal = np.max(self.imageOrig)
        if highLim < highVal:
            self.gui.dsbUpperLimit["value"] = float(highLim)
        else:
            self.gui.dsbUpperLimit["value"] = float(highVal)
        self.gui.histogram.call("deletePicker", -1)
        self.gui.histogram.call("appendPicker", [self.gui.dsbLowerLimit["value"], self.gui.dsbUpperLimit["value"]])
            
    @ItomUi.autoslot("")
    def on_pbClip2s_clicked(self):
        self.adjustLimitsSDev(2)
        self.updateGraph()
        
    @ItomUi.autoslot("")
    def on_pbClip3s_clicked(self):
        self.adjustLimitsSDev(3)
        self.updateGraph()

if(__name__ == '__main__'):
    histClipping = histogramClipping(hasBASICFILTERS)
    histClipping.init(False, "")
    histClipping.show()