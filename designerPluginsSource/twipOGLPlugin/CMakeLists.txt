#********************************************************************
#    twipOGLFigure-Plugin for itom
#    URL: http://www.twip-os.com
#    Copyright (C) 2014, twip optical solutions GmbH, 
#    Stuttgart, Germany 
#
#    This files is part of the designer-Plugin twipOGLFigure for the 
#    measurement software itom. All files of this plugin, where not stated
#    as port of the itom sdk, fall under the GNU Library General 
#    Public Licence and must behandled accordingly.
#
#    twipOGLFigure is free software; you can redistribute it and/or modify it
#    under the terms of the GNU Library General Public Licence as published by
#    the Free Software Foundation; either version 2 of the Licence, or (at
#    your option) any later version.
#
#    Information under www.twip-os.com or mail to info@twip-os.com.
#
#    This itom-plugin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    itom is free software by ITO, University Stuttgart published under 
#    GNU General Public License as published by the Free Software 
#    Foundation. See <https://bitbucket.org/itom/> 
#
#    You should have received a copy of the GNU Library General Public License
#    along with itom. If not, see <http://www.gnu.org/licenses/>.
#*********************************************************************** */

set(target_name twipOGLPlugin)

option(BUILD_TARGET64 "Build for 64 bit target if set to ON or 32 bit if set to OFF." ON)
set(ITOM_SDK_DIR "" CACHE PATH "base path to itom_sdk")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${ITOM_SDK_DIR}/cmake)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

include("ItomBuildMacros")
itom_init_cmake_policy(3.12)
itom_init_designerplugin_library(${target_name})

find_package(ITOM_SDK COMPONENTS dataobject itomCommonLib itomCommonQtLib pointCloud itomCommonPlotLib REQUIRED)
itom_find_package_qt(ON Core Widgets Xml Designer LinguistTools OpenGL OpenGLExtensions)
find_package(OpenCV COMPONENTS core imgproc REQUIRED)
find_package(OpenGL REQUIRED)

if(BUILD_WITH_PCL)
    find_package(PCL 1.5.1 REQUIRED COMPONENTS common)
    add_definitions(-DUSEPCL -D_USEPCL)
endif(BUILD_WITH_PCL)

add_definitions(-DUSESWRANGLERGL)
add_definitions(-DITOMSHAREDDESIGNER)

include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${ITOM_SDK_INCLUDE_DIRS}
    ${CMAKE_CURRENT_SOURCE_DIR}/icons
    ${PCL_INCLUDE_DIRS}
    ${CMAKE_CURRENT_SOURCE_DIR}/..
)

if(UNIX)
    include_directories(
        usr/include #for GL/GL.h and GL/GLU.h
    )
endif(UNIX)


link_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/..
    #
)

set(DESIGNER_HEADERS
    ${ITOM_SDK_INCLUDE_DIR}/common/apiFunctionsGraphInc.h
    ${ITOM_SDK_INCLUDE_DIR}/common/apiFunctionsInc.h
    ${ITOM_SDK_INCLUDE_DIR}/common/sharedStructures.h
    ${ITOM_SDK_INCLUDE_DIR}/common/sharedStructuresGraphics.h
    ${ITOM_SDK_INCLUDE_DIR}/common/sharedStructuresQt.h
    ${ITOM_SDK_INCLUDE_DIR}/common/typeDefs.h
    ${ITOM_SDK_INCLUDE_DIR}/DataObject/dataObjectFuncs.h
    ${ITOM_SDK_INCLUDE_DIR}/plot/AbstractDObjPCLFigure.h
    ${ITOM_SDK_INCLUDE_DIR}/plot/AbstractFigure.h
    ${ITOM_SDK_INCLUDE_DIR}/plot/AbstractItomDesignerPlugin.h
    ${ITOM_SDK_INCLUDE_DIR}/plot/AbstractNode.h
    ${CMAKE_CURRENT_SOURCE_DIR}/framework.h
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLPlugin.h
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLFigure.h
    ${CMAKE_CURRENT_SOURCE_DIR}/shaderEngines.h
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLWidget.h
    ${CMAKE_CURRENT_SOURCE_DIR}/../commonTwip/aboutTwip.h
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLLegend.h
    ${CMAKE_CURRENT_SOURCE_DIR}/pluginVersion.h
    ${CMAKE_CURRENT_BINARY_DIR}/gitVersion.h
)

set(DESIGNER_SOURCES 
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLPlugin.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLFigure.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLWidget.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLLegend.cpp
)

#If you want to use automatical metadata for dlls under windows use the following if-case.
if(MSVC)
    list(APPEND DESIGNER_SOURCES ${ITOM_SDK_INCLUDE_DIR}/../designerPluginLibraryVersion.rc)
endif() 

set(DESIGNER_UI
    ${CMAKE_CURRENT_SOURCE_DIR}/../commonTwip/aboutTwip.ui
)

set(DESIGNER_RCC
    ${CMAKE_CURRENT_SOURCE_DIR}/../twipDesignerPlugins.qrc
    ${CMAKE_CURRENT_SOURCE_DIR}/twipOGLFigure.qrc
)

add_library(${target_name} SHARED ${DESIGNER_SOURCES} ${DESIGNER_HEADERS} ${DESIGNER_UI} ${DESIGNER_RCC} ${EXISTING_TRANSLATION_FILES})

target_link_libraries(${target_name} ${OpenCV_LIBS} ${OPENGL_LIBRARIES} ${ITOM_SDK_LIBRARIES} ${QT5_LIBRARIES} ${VISUALLEAKDETECTOR_LIBRARIES})

# Qt: enable all automoc, autouic and autorcc. Autouic file will be disabled for all files that are processed manually by itom_qt_wrap_ui.
set_target_properties(${target_name} PROPERTIES AUTOMOC ON AUTORCC ON AUTOUIC ON)

# manually parse the ui files to obtain the output files for the translation below
itom_qt_wrap_ui(DESIGNER_UIC ${target_name} ${DESIGNER_UI})

# translations
set(FILES_TO_TRANSLATE ${DESIGNER_SOURCES} ${DESIGNER_HEADERS} ${DESIGNER_UI} ${DESIGNER_RCC})
itom_library_translation(
    QM_FILES 
    TARGET ${target_name} 
    FILES_TO_TRANSLATE ${FILES_TO_TRANSLATE}
    )

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/docs/doxygen/doxygen.dox.in ${CMAKE_CURRENT_BINARY_DIR}/docs/doxygen/doxygen.dox )

# COPY SECTION
set(COPY_SOURCES "")
set(COPY_DESTINATIONS "")
itom_add_designerlibrary_to_copy_list(${target_name} COPY_SOURCES COPY_DESTINATIONS)
itom_add_designer_qm_files_to_copy_list(QM_FILES COPY_SOURCES COPY_DESTINATIONS)
itom_post_build_copy_files(${target_name} COPY_SOURCES COPY_DESTINATIONS)