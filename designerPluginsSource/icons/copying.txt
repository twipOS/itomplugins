/* ********************************************************************
    designerPlugins-icons for itom
    URL: http://www.twip-os.com
    Copyright (C) 2014, twip optical solutions GmbH, 
    Stuttgart, Germany 

    These icons,
    (
        e.g.
        logoSmall.png
        topoTwip.png
        twip.png
        aspRatio.png
        bgColor.png
        changeColor.png
        colorBar.png
        colorBarOff.png
        grid.png
        grid_or.png
        home.png
        home_or.png
        info.png
        rescale.png
        rescale_or.png
        copyimg.png
        ioIcon.png
        ioIcon_or.png
    ),
    are part of the public twip designer-plugin repository for the 
    measurement software itom. This file fall under the GNU Library General 
    Public Licence and must behandled accordingly.

    you can redistribute it and/or modify it
    under the terms of the GNU Library General Public Licence as published by
    the Free Software Foundation; either version 2 of the Licence, or (at
    your option) any later version.

    Information under www.twip-os.com or mail to info@twip-os.com.

    This itom-plugin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    itom is free software by ITO, University Stuttgart published under 
    GNU General Public License as published by the Free Software 
    Foundation. See <https://bitbucket.org/itom/> 

    You should have received a copy of the GNU Library General Public License
    along with itom. If not, see <http://www.gnu.org/licenses/>.
*********************************************************************** */