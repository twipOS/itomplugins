![img](https://bitbucket.org/repo/oA9Kg5/images/2576252462-twip_logo.png)

Welcome to the twip optical solutions GmbH public plugin and designer plugin repository for itom, the ito measurement programm. 


### What is this repository for? ###

* This repository will contain free software plugins for itom
* Itom is a free software under LGPL developed by ito in cooperation with the twip optical solutions GmbH. The source code is hosted [here](http://itom.bitbucket.org/) at bitbucket.
* At the moment this repository contains a openGL based figure for isometric plotting.


### How do I get set up? ###

To compile the plugin you will need the following programms and dependencies

* cmake
* git
* c++-Compiler, e.g. VisualStudio / VisualStudioExpress, QtCreator, ...
* An installed or compiled itom including the itom SDK.
* openGL-Glew
* If pointclouds shall be plotted, the pointcloudlibrary (PCL) by willow garage

### Contribution guidelines ###

* At the moment we have no guidelines for contribution to our plugins
* For contribution to itom, please contact ito

### Who do I talk to? ###

* For questions concerning this repository contact the twip optical solutions GmbH, e.g. via info<at>twip-os.com
* For questions concerning itom contact the twip optical solutions GmbH, e.g. via info<at>twip-os.com or ito.
* For legal terms and furhter questions check out our webpage [twip-os.com](http://www.twip-os.com/index.php?lang=en)